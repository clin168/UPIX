#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void usage(void);
void rm_suffix(void);

int main(int argc, char *argv[])
{
	if ((argc < 2)||(argc > 3)) usage();

/* 1. Check for a null string; POSIX does not specify whether to return a null
string or ".". In either case, stop. */
	if (*argv[1] == '\0')
	{puts(".");exit(0);}

/* 2. If the second argument is just "//", it is implementation-defined whether
the string is processed or left untouched. This just prints "//". */
	else if (strcmp(argv[1],"//") == 0)
	{
		puts("//");
		exit(0);
	}

/* 3. A string that contains just forward slashes is shrunken to a string with
just one. Stop. */
	for (; *argv[1] == '/'; argv[1]++)
	{
		puts("/");
		exit(0);
	}

/* 4. If there are trailing slashes, they are removed. */
	char *basename = argv[1];
	while (strrchr(argv[1],'/')) *argv[1]--  = '\0';

/* 5. If there are slashes that remain, everything up to the last slash is
removed. */

/* 6. If a suffix operand is present, is not identical to the string, and
matches the suffix in the string, the suffix from the string is removed.
Otherwise, the string is not changed. It is not an error if the suffix operand
does not match with the string. */
	char *suffix = argv[2];

	if (argv[2])
		{
			// char suffix = *argv[2];
			int offset = strlen(argv[1]) - strlen(argv[2]);
			
			
			if ((strcmp(suffix,basename) < 0)) suffix == '\0';
		}
		
		
		
	printf("%s\n",basename);
}

void usage(void)
{
	puts("Usage: basename pathname [prefix]");
	exit(1);
}
