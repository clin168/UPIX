#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

int main(void)
{
	char buf[BUFSIZ * 9];
	pid_t pid; int status;

	puts("Work in progress: press ^D to exit shell.");
	
	while(getline(NULL, 0, stdin) != -1) 
	{
		if(buf[strlen(buf)-1] == '\n')
buf[strlen(buf)-1] = 0;

		if((pid = fork()) < 0) fputs("fork error", stderr);
		else if (pid == 0)
		{
			execlp(buf, buf, NULL);
			exit(127);
		}
		if((pid = waitpid(pid, &status, 0)) < 0 )
fputs("waitpid error", stderr);
		if(fgetc(stdin) == EOF) {puts("exit suceeded"); return(0);}
	}
}