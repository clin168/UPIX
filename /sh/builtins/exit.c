#include <stdio.h>
#include <stdlib.h>

unsigned short exitcmd(int argc, char** argv) // Help?
{
	unsigned long status = strtoul(*argv,NULL,10);
	if (status > 255)
	fprintf(stderr, "Exit values that are not between 0 and 255 inclusive \
are undefined by POSIX.");
	else return status;
	return 0; argc++;
}