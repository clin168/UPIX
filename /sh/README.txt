Some of the code is derived from lsh, whose code is found at
<https://github.com/brenns10/lsh>. As such, such code is in the public domain.

Structure:

sc.c      core component. It was named sc to reflect the Unix tradition of
naming with as few letters as possible.
builtins/ source code of shell built-in commands.