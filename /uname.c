#include <unistd.h>
#include <sys/utsname.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

int main(int argc, char **argv)
{
	short int opt;
	short int hwtype_flag, machinename_flag, relflag, osflag, verflag = 0;

	while ((opt = getopt (argc,argv,"amnrsv")) != -1){
	switch (opt)
	{
		case "a":
			hwtype_flag=1; machinename_flag=1; relflag=1; osflag=1; verflag=1;
			break;
		case "m": hwtype_flag=1;
		case "n": machinename_flag=1;
		case "r": relflag=1;
		case "s": osflag=1;
		case "v": verflag=1;
		default: exit(1);
	}}
	uname(utsname);
}

// if (osflag || !(hwtype_flag || relflag || verflag || machinename_flag))

// printf("%s %s %s %s %s\n", <sysname>, <nodename>, release, version, <machine>);
