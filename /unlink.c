/* (C) 2017 Charlie Lin
12/9/2017: First complete version.
*/

#ifdef _XOPEN_SOURCE

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv)
{
	if (argc!=2)
	{printf("Usage: %s files\n",argv[0]);exit(1);}

	unlink(argv[1]);
	exit(0);
}

#endif