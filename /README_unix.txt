At some point, I plan to reorganize contents in the same directory as 
this file...

Also, to view Klingon glyphs in the man pages, you need a font that can 
display them.

See TODO_unix.txt or TODO_win.txt.

-Charlie Lin
