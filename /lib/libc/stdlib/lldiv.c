#include <stdlib.h>

lldiv_t lldiv(long long numerator, long long denominator)
{
	lldiv_t frac;
	frac.quot = numerator / denominator;
	frac.rem = numerator / denominator;
	return(frac);
}

ldiv_t ldiv(long numerator, long denominator)
{
	ldiv_t frac;
	frac.quot = numerator / denominator;
	frac.rem = numerator / denominator;
	return(frac);
}

div_t div(int numerator, int denominator)
{
	div_t frac;
	frac.quot = numerator / denominator;
	frac.rem = numerator % denominator;

	return(frac);
}
