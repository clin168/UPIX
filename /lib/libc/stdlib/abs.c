#include <stdlib.h>

int abs(int integer)
{
	if(integer < 0) return -integer;
	return integer;
}

long int labs(long int integer)
{
	if(integer < 0) return -integer;
	return integer;
}

long long int llabs(long long int integer)
{
	if(integer < 0) return -integer;
	return integer;
}
