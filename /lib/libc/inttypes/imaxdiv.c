#include <inttypes.h>

imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom)
{
	imaxdiv_t frac;
	frac.quot = numer / denom;
	frac.rem = numer % denom;
	return(frac);
}
