#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/* Assumes that the host is little-endian: x86, amd64 */

uint32_t htonl(uint32_t hostlong)
{
	char buf[33];
	sprintf(buf, "%l", hostlong);

	unsigned char i;
	for(i=0;i<=32;++i)
	{
		buf[i] = buf[32-i];
	}

	return((uint32_t) atoi(buf));
}

uint16_t htons(uint16_t hostshort)
{
	char buf[17];
	sprintf(buf, "%h", hostshort);

	unsigned char i;
	for(i=0;i<=16;++i)
	{
		buf[i] = buf[16-i];
	}

	return((uint16_t) atoi(buf));
}

uint32_t ntohl(uint32_t netlong)
{
	char buf[33];
	sprintf(buf, "%l", netlong);

	unsigned char i;
	for(i=0;i<=32;++i)
	{
		buf[i] = buf[32-i];
	}

	return((uint32_t) atoi(buf));
}

uint16_t ntohs(uint16_t netshort)
{
	char buf[17];
	sprintf(buf, "%h", netshort);

	unsigned char i;
	for(i=0;i<=16;++i)
	{
		buf[i] = buf[16-i];
	}

	return((uint16_t) atoi(buf));
}