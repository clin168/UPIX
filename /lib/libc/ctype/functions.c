/* (C) Charlie Lin
First completed: 2/15/2018
Add comments to explain purpose of functions: 2/22/2018
*/

/* On glibc, the functions are macros;
that makes function declarations a pain...
comment out the include statement for now... */

//#include <ctype.h>

/* ASCII value (a): 97.  */
int islower(int c)
{
	return (unsigned)c-'a' < 26;
}

/* ASCII value (A): 65 */
int isupper(int c)
{
	return (unsigned)c-'A' < 26;
}

int isalpha(int c)
{
	return (isupper(c) || islower(c));
}

/* ASCII (0): 48 to ASCII(9): 57 */
int isdigit(int c)
{
	return (unsigned) c > 47 && c < 58;
}

int isalphanum(int ch)
{
	return (isalpha(ch) || isdigit(ch));
}

// Needs better solution.
int isxdigit(int c)
{
	return (isdigit(c) || ((unsigned)c <= 102 && (unsigned)c >= 97) ||
		((unsigned)c >= 65 && (unsigned)c <= 70));
}

// Returns 1 if c is either space or (horizontal) tab, else return 0
int isblank(int c)
{
	return (c == ' ' || c == '\t') ? 1 : 0;
}

/* ASCII control characters are below 32 and the escape character has value of
127 */
int iscntrl(int c)
{
	return ((unsigned)c < 32) || (c = 127);
}

// Almost the inverse of iscntrl(): tests for a printable character
int isgraph(int c)
{
	return (unsigned) c > 32 && c < 127;
}

// Same as above function, but space is included
int isprint(int c)
{
	return (unsigned) c >= 32 && c < 127;
}

// Shortcut: returns inverse of isalphanum
int ispunct(int c)
{
	return !(isalphanum(c) && iscntrl(c));
}

/* Checks for whitespace: space, vertical tab (\v), carriage return (\r),
newline (\n), and form-feed (\f) */
int isspace(int c)
{
	return (isblank(c) || c == '\v' || c == '\r' || c == '\n' || c == '\f');
}

/* If c is in uppercase, add 32 to get its lowercase counterpart, else return
as is */
int tolower(int c)
{
	if (isupper(c)) return c + 32; else return c;
}

/* If c is in lowercase, subtract 32 to get its lowercase counterpart, else
return as is */
int toupper(int c)
{
	if (islower(c)) return c - 32; else return c;
}

#ifdef _XOPEN_SOURCE
/* Obsolete: PLEASE DO NOT USE! */
#define _toupper(c) toupper(c) // Aliases of fuctions not prefixed with underscores
#define _tolower(c) tolower(c)
// Checks if c has value below 128, as ASCII characters have such values
int isascii(int c) {return (unsigned)c < 128;}
#endif

// POSIX locale functions
#ifdef _POSIX_C_SOURCE
#endif