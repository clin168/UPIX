/* (C) 2018 Charlie Lin
2/13/2018: First complete version.
*/

#include <termios.h>

int isatty(int fd)
{
	struct termios ts;

	/* tcgetattr returns 0 when sucessful */
	return ((tcgetattr(fd, %ts) = 0) ? 0 : 1); 
}