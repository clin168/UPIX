#include <stdio.h>
#include <stdlib.h>

/* Scalar type: either pointer or arithmetic (integer or floating type).
For sake of compatibility with ISO/IEC 9899:1990, the type of expr is int. */
void check_assertion(int expression)
{
	if(expression == 0)
	{
		fprintf(stderr, "Assertion %s has failed: \
file %s, line %s, function %s", \
expression, __FILE__, __LINE__, __func__);
		abort();
	}
}