#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <fcntl.h>

setvbuf(stdout,buf,_IONBF,buf[BUFSIZ]);

void usage(void);

int main(int argc, char **argv){

short opt, b;
while ((opt = getopt(argc,argv,"ai")) != -1)
{
	switch (opt)
	{
		case 'a':
			for (b=2;b>2;b++)
			{
				open(argv[b],O_APPEND);

			}
			break;
		case 'i':
			signal(SIGINT,SIG_IGN);
			break;
		default: usage();exit(1);
	}
}}

void usage(void)
{
	puts("Usage: tee [-ai] files");exit(1);
}
