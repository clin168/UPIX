#ifdef _XOPEN_SOURCE
/* (C) Charlie Lin
3/16/2018: first complete implementation.
*/
#include "../private/size_t.h"
#include "../private/ssize_t.h"

struct iovec {
	void *iov_base; // Base address of a memory region for I/O.
	size_t iov_len; // Size of memory pointed to by iov_base.
};

ssize_t readv(int, const struct iovec *, int);
ssize_t writev(int, const struct iovec *, int);
#endif
