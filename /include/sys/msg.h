#ifdef _XOPEN_SOURCE
/* (C) Charlie Lin
3/16/2018: first complete implementation.
*/
#include "../private/pid_t.h"
#include "../private/size_t.h"
#include "../private/ssize_t.h"
#include "../private/time_t.h"
#include <sys/ipc.h>

#define MSG_NOERROR 1 // No error emitted if big message.

typedef unsigned short msgqnum_t; // Number of messages in message queue.
typedef unsigned short msglen_t;  // Number of bytes allowed in message queue. 

struct msqid_ds{
struct ipc_perm msg_perm;  // Operation permission structure. 
msgqnum_t       msg_qnum;  // Number of messages currently on queue. 
msglen_t        msg_qbytes; // Maximum number of bytes allowed on queue. 
pid_t           msg_lspid;  // Process ID of last msgsnd(). 
pid_t           msg_lrpid; // Process ID of last msgrcv(). 
time_t          msg_stime;  // Time of last msgsnd(). 
time_t          msg_rtime;  // Time of last msgrcv(). 
time_t          msg_ctime;  // Time of last change. 
};

int       msgctl(int, int, struct msqid_ds *);
int       msgget(key_t, int);
ssize_t   msgrcv(int, void *, size_t, long, int);
int       msgsnd(int, const void *, size_t, int);

#endif
