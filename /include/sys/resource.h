#ifdef _XOPEN_SORCE

#include "../private/timeval.h"
#include "../private/id_t.h"

typedef unsigned rlim_t;

struct rlimit
{
	rlim_t rlim_cur; // Current (soft) limit
	rlim_t rlim_max; // Hard limit
}

struct rusage
{
	struct timeval ru_utime; // User time used
	struct timeval ru_stimel; // System time used
}

int getpriority(int, id_t);
int getrlimit(int, struct rlimit* );
int getrusage(int, struct rusage* );
int setpriority(int, id_t, int);
int setrlimit(int, const struct rlimit* );

#endif
