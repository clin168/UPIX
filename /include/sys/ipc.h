#ifdef _XOPEN_SOURCE
/* (C) Charlie Lin
3/16/2018: first complete implementation.
*/
 #include "../private/ugid_t.h"
 #include "../private/mode_t.h"
 #include "../private/key_t.h"

 #define IPC_CREAT  0 // Create entry if key doesn't exist.
 #define IPC_EXCL   1 // Fail if key exists.
 #define IPC_NOWAIT 2 // Emit a error if request must wait.

 #define IPC_PRIVATE 0 // Private key.

 #define IPC_RMID 0 // Remove identifier.
 #define IPC_SET  1 // Set options.
 #define IPC_STAT 2 // Get options.

 struct ipc_perm
 {
	uid_t uid;
 	gid_t gid;
 	uid_t cuid;
 	gid_t cgid;
 	mode_t mode;	
 }

 key_t ftok(const char*, int);
#endif
