#include "../private/size_t.h"
#include "../private/ssize_t.h"

#define SCM_RIGHTS 0 // Data array contains access rights to be sent or recieved

#define CMSG_DATA(struct cmsghdr*) (unsigned char*)

#define SOCK_DGRAM     0 // Datagram socket
#define SOCK_SEQPACKET 1
#define SOCK_STREAM    2

#define SOL_SOCKET 0

#define SO_ACCEPTCONN 0  // Socket accepts connections.
#define SO_BROADCAST  1  // Transmission of broadcast messages is supported.
#define SO_DEBUG      2  // Debugging information is being recorded.
#define SO_DONTROUTE  3  // Bypass normal routing.
#define SO_ERROR      4  // Socket error status.
#define SO_KEEPALIVE  5  // Connections are kept alive with periodic messages.
#define SO_LINGER     6  // Socket lingers on close.
#define SO_OOBINLINE  7  // Out-of-band data is transmitted in line.
#define SO_RCVBUF     8  // Receive buffer size.
#define SO_RCVLOWAT   9  // Receive ``low water mark''.
#define SO_RCVTIMEO   10 // Receive timeout.
#define SO_REUSEADDR  11 // Reuse of local addresses is supported.
#define SO_SNDBUF     12 // Send buffer size.
#define SO_SNDLOWAT   13 // Send ``low water mark''.
#define SO_SNDTIMEO   14 // Send timeout.
#define SO_TYPE       15 // Socket type.

#define SOMAXCONN // Maximum backlog queue length

#define MSG_CTRUNC 
#define MSG_DONTROUTE // Send without using routing tables.
#define MSG_EOR // Terminates a record (if supported by the protocol).
#define MSG_OOB // Out-of-band data.
#define MSG_NOSIGNAL//  No SIGPIPE generated when an attempt to send is made on a stream-oriented socket that is no longer connected.
#define MSG_PEEK // Leave received data in queue.
#define MSG_TRUNC // Normal data truncated.
#define MSG_WAITALL // Attempt to fill the read buffer.

#define AF_INET   2 // IPv4 internet socket
#define AF_UNIX   1 // UNIX domain sockets
#define AF_UNSPEC 0 // Unspecified socket; must be 0.

#define SHUT_RD
#define SHUt_RDWR
#define SHUT_WR

typedef usigned long socklen_t;
typedef unsigned sa_family_t;

struct sockaddr
{
	sa_family_t sa_family;
	char sa_data[];
}
// XXX: finish
struct sockaddr_storage
{
	sa_family_t ss_family;
}

/* Required for non-XSI systems */
struct iovec
{
	void* iov_base; // Base address of a memory region for I/O
	size_t iov_len; // Size of memory pointed to by iov_base
}

struct msghdr
{
	void*         msg_name;       // Optional address.
	socklen_t     msg_namelen;    // Size of address.
	struct iovec* msg_iov;        // Scatter/gather array.
	int           msg_iovlen;     // Members in msg_iov.
	void*         msg_control;    // Ancillary data; see below.
	socklen_t     msg_controllen; // Ancillary data buffer len.
	int           msg_flags;      // Flags on received message.
}

struct cmsghdr
{
	socklen_t cmesg_len;   // Byte count, including cmsghdr.
	int       cmesg_level; // Originating protocol.
	int       cmesg_type;  // Protocol-specific type.
}

struct linger
{
	int l_onoff;  // Linger option is enabled.
	int l_linger; // Linger time in seconds.
}

int accept(int, struct sockaddr *restrict, socklen_t *restrict);
int bind(int, const struct sockaddr *, socklen_t);
int connect(int, const struct sockaddr *, socklen_t);
int getpeername(int, struct sockaddr *restrict, socklen_t *restrict);
int getsockname(int, struct sockaddr *restrict, socklen_t *restrict);
int getsockopt(int, int, int, void *restrict, socklen_t *restrict);
int listen(int, int);
ssize_t recv(int, void *, size_t, int);
ssize_t recvfrom(int, void *restrict, size_t, int, struct sockaddr *restrict, socklen_t *restrict);
ssize_t recvmsg(int, struct msghdr *, int);
ssize_t send(int, const void *, size_t, int);
ssize_t sendmsg(int, const struct msghdr *, int);
ssize_t sendto(int, const void *, size_t, int, const struct sockaddr *, socklen_t);
int setsockopt(int, int, int, const void *, socklen_t);
int shutdown(int, int);
int sockatmark(int);
int socket(int, int, int);
int socketpair(int, int, int, int [2]);
