/* (C) Charlie Lin
3/16/2018: first complete XSI-conformant implementation that lacks most POSIX
memory options.
*/

#include "../private/mode_t.h"
#include "../private/off_t.h"
#include "../private/size_t.h"

#if defined _XOPEN_SOURCE || defined _POSIX_SYNCHRONIZED_IO
 #define MS_ASYNC      0 // Asynchronous writes.
 #define MS_INVALIDATE 1 // Invalidate mappings.
 #define MS_SYNC       2 // Synchronous writes.

 int msync(void *, size_t, int);
#endif

#define PROT_EXEC  0  // Page can be executed.
#define PROT_NONE  1  // Page cannot be accessed.
#define PROT_READ  2  // Page can be read.
#define PROT_WRITE 3  // Page can be written.

#define MAP_FIXED   0 // Interpret addr exactly.
#define MAP_PRIVATE 1 // Changes are private.
#define MAP_SHARED  2 // Changes are sharable.

#define MAP_FAILED ((void*)0)

void *mmap(void *, size_t, int, int, int, off_t);
int mprotect(void *, size_t, int);
int munmap(void *, size_t);
