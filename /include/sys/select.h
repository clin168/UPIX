#define FD_SETSIZE 48 // !complete

struct timeval {
time_t         tv_sec;      // Seconds. 
suseconds_t    tv_usec;     // Microseconds. 
};

void FD_CLR(int, fd_set *);
int  FD_ISSET(int, fd_set *);
void FD_SET(int, fd_set *);
void FD_ZERO(fd_set *);

int  pselect(int, fd_set *restrict, fd_set *restrict, fd_set *restrict, \
const struct timespec *restrict, const sigset_t *restrict);
int  select(int, fd_set *restrict, fd_set *restrict, fd_set *restrict, \
struct timeval *restrict);
