#include <locale.h>

#include <sys/types.h>

int strcasecmp (const char *, const char *);
int strncasecmp (const char *, const char *, size_t);
int strcasecmp_l (const char *, const char *, locale_t);
int strncasecmp_l (const char *, const char *, size_t, locale_t);

#ifdef _XOPEN_SOURCE
int ffs(int);
#endif
