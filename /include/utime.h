/* OBSOLETE: PLEASE do not use! */
#include "private/time_t.h"

struct utimbuf
{
	time_t actime;  // Access time
	time_t modtime; // Modification time
}

int utime(const char *, const struct utimbuf *);