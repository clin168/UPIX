// !complete
void longjmp(jmp_buf, int);
int setjmp(jmp_buf);

#ifdef _POSIX_SOURCE
void siglongjmp(sigjmp_buf, int);
int sigsetjmp(sigjmp_buf, int);
#endif

#ifdef _XOPEN_SOURCE
/* Obselete: PLEASE do not use them! */
void _longjmp(jmp_buf, int);
int _setjmp(jmp_buf);
#endif
