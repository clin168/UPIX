#include <sys/types.h>

struct group {
	char *gr_name;
	gid_t gr_gid;
	char **gr_mem;
};

#ifdef _XOPEN_VERSION
 void endgrent(void);
 struct group *getgrent(void);
 void setgrent(void);
#endif

struct group *getgrgid(gid_t);
int getgrgid_r(gid_t, struct group *, char *, size_t, struct group **);
struct group *getgrnam(const char *);
int gengrnam_r(const char *, struct group *, char *, size_t, struct group **);
