#define TMAGIC "ustar"
#define TMAGLEN 6 // Length of TMAGIC+'\0'
#define TVERSION "00"
#define TVERSLEN 2

#define REGTYPE   '0' // Regular file.
#define AREGTYPE '\0' // Regular file.
#define LNKTYPE   '1' // Hard link.
#define SYMTYPE   '2' // Symbolic link.
#define CHRTYPE   '3' // Character device node.
#define BLKTYPE   '4' // Block device node.
#define DIRTYPE   '5' // Directory.
#define FIFOTYPE  '6' // FIFO (named pipe).
#define CONTTYPE  '7' // Reserved.

#define TSUID 04000 // Set UID on execution.
#define TSGID 02000 // Set GID on execution.

#ifdef _XOPEN_SOURCE
#define TSVTX 01000 // On directories, restricted deletion flag. 
#endif

#define TUREAD  00400 // Readable by owner.
#define TUWRITE 00200 // Writable by owner.
#define TUEXEC  00100 // Executable/searchable by owner.
#define TGREAD  00040 // Readable by owning group.
#define TGWRITE 00020 // Writeable by owning group.
#define TGEXEC  00010 // Executable/searchable by owning group.
#define TOREAD  00004 // Readable by others.
#define TOWRITE 00002 // Writable by others.
#define TOEXEC  00001 // Executable/searchable by others.
