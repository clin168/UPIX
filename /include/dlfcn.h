//define constants // !complete
#define RTLD_LAZY
#define RTLD_NOW
#define RTLD_GLOBAL
#define RTLD_LOCAL

int dlclose(void *);
char *dlerror(void);
void *dlopen(const char *, int);
void *dlsym(void *restrict, const char *restrict);