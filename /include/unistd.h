#include <stdint.h> // !complete

#include <sys/types.h>

#define _POSIX_VERSION 200809L
#define _POSIX2_VERSION 200809L

#define _XOPEN_VERSION 700

/* Options */
#define _POSIX_ADVISORY_INFO
#define _POSIX_ASYNCHRONOUS_IO 200809L
#define _POSIX_BARRIERS 200809L
#define _POSIX_CHOWN_RESTRICTED 0
#define _POSIX_CPUTIME
#define _POSIX_FSYNC 0
#define _POSIX_IPV6
#define _POSIX_JOB_CONTROL 1
#define _POSIX_MAPPED_FILES 200809L
#define _POSIX_MEMLOCK
#define _POSIX_MEMLOCK_RANGE
#define _POSIX_MEMORY_PROTECTION 200809L
#define _POSIX_MESSAGE_PASSING
#define _POSIX_MONOTONIC_CLOCK
#define _POSIX_PRIORITIZED_IO
#define _POSIX_PRIORITY_SCHEDULING
#define _POSIX_RAW_SOCKETS
#define _POSIX_REALTIME_SIGNALS 200809L
#define _POSIX_REGEXP 1
#define _POSIX_SAVED_IDS 1
#define _POSIX_SEMAPHORES 200809L
#define _POSIX_SHARED_MEMORY_OBJECTS
#define _POSIX_SHELL 1
#define _POSIX_SPAWN
#define _POSIX_SPIN_LOCKS 200809L
#define _POSIX_SPORADIC_SERVER
#define _POSIX_SYNCHRONIZED_IO
#define _POSIX_THREAD_ATTR_STACKADDR 0
#define _POSIX_THREAD_ATTR_STACKSIZE 0
#define _POSIX_THRWAD_CPUTIME
#define _POSIX_THREAD_PRIO_INHERIT
#define _POSIX_THREAD_PRIO_PROTECT
#define _POSIX_THREAD_PRIORITY_SCHEDULING
#define _POSIX_THREAD_PROCESS_SHARED 0
#define _POSIX_THREAD_ROBUST_PRIO_INHERIT
#define _POSIX_THREAD_ROBUST_PRIO_PROTECT
#define _POSIX_THREAD_SAFE_FUNCTIONS 200809L
#define _POSIX_SPORADIC_SERVER
#define _POSIX_THREADS 200809L
#define _POSIX_TIMEOUTS 200809L
#define _POSIX_TIMERS 200809L
#define _POSIX_TRACE 0
#define _POSIX_TRACE_EVENT_FILTER 0
#define _POSIX_TRACE_INHERIT 0
#define _POSIX_TRACE_LOG 0
#define _POSIX_TYPED_MEMORY_OBJECTS -1
#define _POSIX_V6_ILP32_OFF32 100
#define _POSIX_V6_ILP32_OFFBIG 102
#define _POSIX_V6_LP64_OFF64 104
#define _POSIX_V6_LPBIG_OFFBIG 600
#define _POSIX_V7_ILP32_OFF32 160
#define _POSIX_V7_ILP32_OFFBIG 500
#define _POSIX_V7_LP64_OFF64 106
#define _POSIX_V7_LPBIG_OFFBIG 123
#define _POSIX2_C_BIND 200809L
#define _POSIX2_C_DEV 0
#define _POSIX2_CHAR_TERM 200809L
#define _POSIX2_FORT_DEV -1
#define _POSIX2_FORT_RUN -1
#define _POSIX2_LOCALEDEF 0
#define _POSIX2_PBS 0
#define _POSIX2_PBS_ACCOUNTING 0
#define _POSIX2_PBS_CHECKPOINT 0
#define _POSIX2_PBS_LOCATE 0
#define _POSIX2_PBS_MESSAGE 0
#define _POSIX2_SW_DEV 0
#define _POSIX2_UPE 0
#define _XOPEN_CRYPT 0
#define _XOPEN_ENH_I18N 0
#define _XOPEN_REALTIME 0
#define _XOPEN_REALTIME_THREADS 0
#define _XOPEN_SHM 0
#define _XOPEN_STREAMS // Obselete
#define _XOPEN_UNIX 0
#define _XOPEN_UUCP 0


#define F_OK 4 // Test for existence of file.
#define R_OK 2// Test for read permission.
#define W_OK 1// Test for write permission.
#define X_OK 0// Test for execute (search) permission.



#define STDIN_FILENO  0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

/* Issue 6: obsolete */

#define _CS_POSIX_V6_ILP32_OFF32_CFLAGS
#define _CS_POSIX_V6_ILP32_OFF32_LDFLAGS
#define _CS_POSIX_V6_ILP32_OFF32_LIBS
#define _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS
#define _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS
#define _CS_POSIX_V6_ILP32_OFFBIG_LIBS
#define _CS_POSIX_V6_LP64_OFF64_CFLAGS
#define _CS_POSIX_V6_LP64_OFF64_LDFLAGS
#define _CS_POSIX_V6_LP64_OFF64_LIBS
#define _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS
#define _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS
#define _CS_POSIX_V6_LPBIG_OFFBIG_LIBS
#define _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS
#define _CS_V6_ENV

#define STDERR_FILENO 2 // File number of stderr; 2.
#define STDIN_FILENO 0 // File number of stdin; 0.
#define STDOUT_FILENO 1 // File number of stdout; 1.

#define _POSIX_VDISABLE 0

#ifdef _XOPEN_SOURCE
#define F_LOCK 0 // Lock a section for exclusive use.
#define F_TEST 1 // Test section for locks by other processes.
#define F_TLOCK 2 // Test and lock a section for exclusive use.
#define F_ULOCK 3 // Unlock locked sections.
#endif

extern char *optarg;
extern int opterr, optind, optopt;

int access(const char *, int);
unsigned alarm(unsigned);
int chdir(const char *);
int chown(const char *, uid_t, gid_t);
int close(int);
size_t confstr(int, char *, size_t);
int dup(int);
int dup2(int, int);
void _exit(int);
int execl(const char *, const char *, ...);
int execle(const char *, const char *, ...);
int execlp(const char *, const char *, ...);
int execv(const char *, char *const []);
int execve(const char *, char *const [], char *const []);
int execvp(const char *, char *const []);
int faccessat(int, const char *, int, int);
int fchdir(int);
int fchown(int, uid_t, gid_t);
int fchownat(int, const char *, uid_t, gid_t, int);
int fexecve(int, char *const [], char *const []);
pid_t fork(void);
long fpathconf(int, int);
int ftruncate(int, off_t);
char *getcwd(char *, size_t);
gid_t getegid(void);
uid_t geteuid(void);
gid_t getgid(void);
int getgroups(int, gid_t []);
int gethostname(char *, size_t);
char *getlogin(void);
int getlogin_r(char *, size_t);
int getopt(int, char * const [], const char *);
pid_t getpgid(pid_t);
pid_t getpgrp(void);
pid_t getpid(void);
pid_t getppid(void);
pid_t getsid(pid_t);
uid_t getuid(void);
int isatty(int);
int lchown(const char *, uid_t, gid_t);
int link(const char *, const char *);
int linkat(int, const char *, int, const char *, int);
off_t lseek(int, off_t, int);
long pathconf(const char *, int);
int pause(void);
int pipe(int [2]);
ssize_t pread(int, void *, size_t, off_t);
ssize_t pwrite(int, const void *, size_t, off_t);
ssize_t read(int, void *, size_t);
ssize_t readlink(const char *restrict, char *restrict, size_t);
ssize_t readlinkat(int, const char *restrict, char *restrict, size_t);
int rmdir(const char *);
int setegid(gid_t);
int seteuid(uid_t);
int setgid(gid_t);
int setpgid(pid_t, pid_t);
pid_t setsid(void);
int setuid(uid_t);
unsigned sleep(unsigned);
int symlink(const char *, const char *);
int symlinkat(const char *, int, const char *);
long sysconf(int);
pid_t tcgetpgrp(int);
int tcsetpgrp(int, pid_t);
int truncate(const char *, off_t);
char *ttyname(int);
int ttyname_r(int, char *, size_t);
int unlink(const char *);
int unlinkat(int, const char *, int);
ssize_t write(int, const void *, size_t);

#ifdef _POSIX_FSYNC
int fsync(int);
#endif

#ifdef _POSIX_SYNCHRONIZED_IO
int fdatasync(int);
#endif

#ifdef _XOPEN_SYSTEM

/* Obselete: DO NOT use! */
pid_t setpgrp(void);

char *crypt(const char *, const char *);
void encrypt(char [64], int);
long gethostid(void);
int lockf(int, int, off_t);
int nice(int);
int setregid(gid_t, gid_t);
int setreuid(uid_t, uid_t);
void swab(const void *restrict, void *restrict, ssize_t);
void sync(void);
#endif
