// !complete
/* Numerical limits. */
#define CHAR_BIT 8
#define CHAR_MIN // SCHAR_MIN or 0
#define CHAR_MAX // SCHAR_MAX or UCHAR_MAX
#define SCHAR_MIN -127
#define SCHAR_MAX 127
#define UCHAR_MAX 255 // 2^(CHAR_BIT)-1
#define SHRT_MAX 32767
#define SHRT_MIN -32767
#define USHRT_MAX 65535
#ifdef _POSIX_SOURCE
 #define INT_MIN LONG_MIN
 #define INT_MAX LONG_MAX
 #else
 #define INT_MIN SHRT_MIN
 #define INT_MAX SHRT_MAX
#endif
#define LONG_MIN -2147483647
#define LONG_MAX 2147483647
#define ULONG_MAX 4294967295
#define LLONG_MAX 9223372036854775807
#define LLONG_MIN -9223372036854775807
#define ULLONG_MAX 18446744073709551615
#define MB_LEN_MAX 1

#ifdef _POSIX_SOURCE

#define WORD_BIT 32
/* Constants regarding pathnames. */
#define FILESIZEBITS 32
#define LINK_MAX _POSIX_LINK_MAX
#define MAX_CANON _POSIX_MAX_CANON
#define MAX_INPUT _POSIX_MAX_INPUT
#ifdef _XOPEN_SOURCE
 #define NAME_MAX _XOPEN_NAME_MAX
 #define PATH_MAX _XOPEN_PATH_MAX
 #else
 #define NAME_MAX _POSIX_NAME_MAX
 #define PATH_MAX _POSIX_PATH_MAX
#endif
#define PIPE_BUF _POSIX_PIPE_BUF
#define SYMLINK_MAX _POSIX_SYMLINK_MAX

#endif


