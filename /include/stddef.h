#define NULL ((void *)0) // !complete
#define offsetof(type, member) (size_t)

typedef signed int ptrdiff_t;
typedef char wchar_t; // For sake of simplicity
typedef unsigned int size_t;
