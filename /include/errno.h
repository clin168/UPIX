extern int errno;

// Out of domain (mathematical)
#define EDOM 1
#define EILSEQ 2
#define ERANGE 3

#ifdef _POSIX_C_SOURCE
// Argument list is too long.
 #define E2BIG 4
// Permission is denied when accessing a file.
 #define EACCES 5
// Address is in use.
 #define EADDRINUSE 6
// Address is not available.
 #define EADDRNOTAVAIL 7
// Address family is not supported or address is not valid for address family.
 #define EAFNOSUPPORT 8
// Resource is temporarily unavailable.
 #define EAGAIN 9
// Connection is already in progress.
 #define EALREADY 10
// Bad file descriptor: out of range, write-only, or refers to no open file.
 #define EBADF 11
// Corrupted message.
 #define EBADMSG 12
// Device or resource is busy.
 #define EBUSY 13
// Asynchronous operation is canceled before completion.
 #define ECANCELED 14
// No child process.
 #define ECHILD 15
// Connection is aborted.
 #define ECONNABORTED 16
// Connection is refused.
 #define ECONNREFUSED 17
// Connection is reset.
 #define ECONNRESET 18
// Resource deadlock would occur.
 #define EDEADLK 19
// Destination address is required.
 #define EDESTADDRREQ 20
// Reserved.
 #define EDQUOT 21
// File exists.
 #define EEXIST 22
// Bad address.
 #define EFAULT 23
// File is too large.
 #define EFBIG 24
// Host is unreachable.
 #define EHOSTUNREACH 25
// Identifier is removed. (XSI IPC)
 #define EIDRM 26
// Operation in progress or O_NONBLOCK is set for socket fd and connection can't be established.
 #define EINPROGRESS 27
// Asynchronous signal was caught during execution of an interruptible function.
 #define EINTR 28
// Invalid argument.
 #define EINVAL 29
// I/O error.
 #define EIO 30
// Socket is connected.
 #define EISCONN 31
// Is a directory.
 #define EISDIR 32
// Loop is encountered when resolving symbolic links or there are more than SYMLOOP_MAX symbolic links.
 #define ELOOP 33
// fd greater than or equal to {OPEN_MAX}, RLIMIT_NOFILE, or too many open streams.
 #define EMFILE 34
// Link count exceeds {LINK_MAX}.
 #define EMLINK 35
// Message is too large or has inappropriate length.
 #define EMSGSIZE 36
// Reserved.
 #define EMULTIHOP 37
// Length of path exceeds {PATH_MAX} or length of filename exceeds {NAME_MAX}.
 #define ENAMETOOLONG 38
// Network is down.
 #define ENETDOWN 39
// Connection is aborted by network.
 #define ENETRESET 40
// Network unreachable.
 #define ENETUNREACH 41
// Too many open files.
 #define ENFILE 42
// No buffer space available for a socket operation/
 #define ENOBUFS 43
// Inappropriate function to a device.
 #define ENODEV 44
// File or directory does not exist.
 #define ENOENT 45
// Error when executing a file.
 #define ENOEXEC 46
// Reserved
 #define ENOLINK 47
// Not enough memory.
 #define ENOMEM 48
// 
 #define ENOMSG 49
// Protocol option to setsockopt() is not supported.
 #define ENOPROTOOPT 50
// No storage space remains.
 #define ENOSPC 51
// Attempt to use optional functionality that is unsupported.
 #define ENOSYS 52
// Socket is not connected.
 #define ENOTCONN 53
// Not a directory.
 #define ENOTDIR 54
// Directory is not empty.
 #define ENOTEMPTY 55
// State from mutex is irrecoverable.
 #define ENOTRECOVERABLE 56
// fd is not a socket.
 #define ENOTSOCK 57
// Feature or value not supported.
 #define ENOTSUP 58

 #define ENOTTY 59
// I/O on device file which refers to non-existant or incapable device.
 #define ENXIO 60
// Operation is not supported on socket.
 #define EOPNOTSUPP 61
// Overflow
 #define EOVERFLOW 62
// Owner of mutex is terminated while holding mutex lock.
 #define EOWNERDEAD 63
// Operation is not permitted.
 #define EPERM 64
// Write attempted on pipe, FIFO, or socket for which there is no process to read data.
 #define EPIPE 65
// Protocol error.
 #define EPROTO 66
// Protocol is not supported.
 #define EPROTONOSUPPORT 67
// Socket type is not supported by protocol.
 #define EPROTOTYPE 68
// Read-only file system.
 #define EROFS 69
 #define ESPIPE 70
// No process is found.
 #define ESRCH 71
 #define ESTALE 72
// 
 #define ETIMEDOUT 73
// 
 #define ETXTBSY 74
// 
 #define EWOULDBLOCK 75
// Attempt to link to a file on another filesystem.
 #define EXDEV 76
#endif
/* Obselete: PLEASE do not use them! */
/* #ifdef _XOPEN_STREAMS
 #define ENODATA 77
 #define ENOSR 78
 #define ENOSTR 79
 #define ETIME 80
#endif */
