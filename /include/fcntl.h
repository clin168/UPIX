#define F_DUPFD 1 // !complete
#define F_DUPFD_EXEC 2
#define F_GETFD 3
#define F_SETFD 4
#define F_GETFL 5
#define F_SETFL 6
#define F_GETLK 7
#define F_SETLK 8
#define F_SETLKW 9
#define F_GETOWN 10
#define F_SETOWN 11

#define FD_CLOEXEC 20

#define F_RDLCK 1
#define F_UNLCK 2
#define F_WRLCK 3

#define O_CLOEXEC 0
#define O_CREAT 1
#define O_DIRECTORY 2
#define O_EXCL 3
#define O_NOCTTY 4
#define O_NOFOLLOW 5
#define O_TRUNC 6
#define O_TTY_INIT 7

#define O_APPEND 1
#define O_NONBLOCK 2
#define O_SYNC 3

#define O_ACCMODE 1

#define O_EXEC 1
#define O_RDONLY 2
#define O_RDWR 3
#define O_SEARCH 4
#define O_WRONLY 5

#define AT_FDCWD 1

#define AT_EACCESS 1

#define AT_SYMLINK_NOFOLLOW 1
#define AT_SYMLINK_FOLLOW 2

#define AT_REMOVEDIR 1

struct flock {
	short l_type;
	short l_whence;
	off_t l_start;
	off_t l_len;
	pid_t l_pid;
};

int  creat(const char *, mode_t);
int  fcntl(int, int, ...);
int  open(const char *, int, ...);
int  openat(int, const char *, int, ...);

#ifdef _POSIX_ADVISORY_INFO

#define POSIX_FADV_DONTNEED 1
#define POSIX_FADV_NOREUSE 2
#define POSIX_FADV_NORMAL 3
#define POSIX_FADV_RANDOM 4
#define POSIX_FADV_SEQUENTIAL 5
#define POSIX_FADV_WILLNEED 6

int  posix_fadvise(int, off_t, off_t, int);
int  posix_fallocate(int, off_t, off_t);

#endif

#ifdef _POSIX_SYNCHRONIZED_IO

#define O_DSYNC 1
#define O_RSYNC 2

#endif
