#define IF_NAMESIZE sizeof(unsigned char)+1

struct if_nameindex{
	unsigned if_index; // Index of interface
	char *if_name;     // Null-terminated name
};

void if_freenameindex(struct if_nameindex *);
char *if_indextoname(unsigned, char *);
struct if_nameindex *if_nameindex(void);
unsigned if_nametoindex(const char *);