#define FNM_NOMATCH 1 // !complete
#define FNM_PATHNAME 2
#define FNM_PERIOD 3
#define FNM_NOESCAPE 4

int fnmatch(const char *, const char *, int);
