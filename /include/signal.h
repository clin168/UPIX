// !complete
#define SIG_DFL 0
#define SIG_ERR 1
#define SIG_IGN 2

#define SIGABRT 1
#define SIGFPE 2
#define SIGILL 3
#define SIGINT 4
#define SIGSEGV 5
#define SIGTERM 6

typedef int sig_atomic_t;

int raise(int);
void (*signal(int, void (*)(int)))(int);

#ifdef _POSIX_SOURCE

#define SIGEV_NONE 0
#define SIGEV_SIGNAL 1
#define SIGEV_THREAD 2

#define SIGRTMIN
#define SIGRTMAX

typedef int sigset_t;
typedef pid_t;

struct sigevent {
	int sigev_notify ;
	int sigev_signo;
	union sigval sigev_value;
	void (*sigev_notify_function)(union sigval);
	pthread_attr_t *sigev_notify_attributes;
};

union sigval {
	int sival_int;
	void *sival_ptr;
};

int kill(pid_t, int);
void psiginfo(const siginfo_t *, const char *);
void psignal(int, const char *);
int pthread_kill(pthread_t, int);
int pthread_sigmask(int, const sigset_t *restrict, sigset_t *restrict);
int sigaction(int, const struct sigaction *restrict, \
struct sigaction *restrict);
int sigaddset(sigset_t *, int);
int sigdelset(sigset_t *, int);
int sigemptyset(sigset_t *);
int sigfillset(sigset_t *);
int sigismember(const sigset_t *, int);
int sigpending(sigset_t *);
int sigprocmask(int, const sigset_t *restrict, sigset_t *restrict);
int sigqueue(pid_t, int, union sigval);
int sigsuspend(const sigset_t *);
int sigtimedwait(const sigset_t *restrict, siginfo_t *restrict, \
const struct timespec *restrict);
int sigwait(const sigset_t *restrict, int *restrict);
int sigwaitinfo(const sigset_t *restrict, siginfo_t *restrict);

#endif

#ifdef _XOPEN_SOURCE
/* Obselete: PLEASE do not use it! */
#define SIG_HOLD 3

/* Obselete: PLEASE do not use them! */
int sighold(int);
int sigpause(int);
int sigrelse(int);

int killpg(pid_t, int);
int sigaltstack(const stack_t *restrict, stack_t *restrict);

#endif
