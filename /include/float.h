// !complete
#define FLT_ROUNDS

#define FLT_EVAL_METHOD

#define FLT_RADIX 2

#define FLT_MANT_DIG
#define DBL_MANT_DIG
#define LDBL_MANT_DIG

#define DECIMAL_DIG 10

#define FLT_DIG 6
#define DBL_DIG 10
#define LDBL_DIG 10

#define FLT_MIN_EXP
#define DBL_MIN_EXP
#define LDBL_MIN_EXP

#define FLT_MIN_10_EXP -37
#define DBL_MIN_10_EXP -37
#define LDBL_MIN_10_EXP -37

#define FLT_MAX_EXP
#define DBL_MAX_EXP
#define LDBL_MAX_EXP

#define FLT_MAX_10_EXP 37
#define DBL_MAX_10_EXP 37
#define LDBL_MAX_10_EXP 37

#define FLT_EPSILON 1e-5
#define DBL_EPSILON 1e-9
#define LDBL_EPSILON 1e-9

#define FLT_MIN 1E-37
#define DBL_MIN 1E-37
#define LDBL_MIN 1E-37
