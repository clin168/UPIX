// !complete
#define GLOB_APPEND 1 // Append generated pathnames to those previously obtained.
#define GLOB_DOOFFS 2 // Specify how many null pointers to add to the beginning of gl_pathv.
#define GLOB_ERR 3 // Cause glob() to return on error.
#define GLOB_MARK 4 // Each pathname that is a directory that matches pattern has a <slash> appended.
#define GLOB_NOCHECK 5 // If pattern does not match any pathname, then return a list consisting of only pattern.
#define GLOB_NOESCAPE 6 // Disable backslash escaping.
#define GLOB_NOSORT 7 // Do not sort the pathnames returned.

#define GLOB_ABORTED 1 // The scan was stopped because GLOB_ERR was set or (*errfunc)() returned non-zero.
#define GLOB_NOMATCH 2 //The pattern does not match any existing pathname, and GLOB_NOCHECK was not set in flags.
#define GLOB_NOSPACE 3 // An attempt to allocate memory failed.

typedef struct {
	size_t gl_pathc;
	char **gl_pathv;
	size_t gl_offs;
} glob_t;

int glob(const char *restrict, int, int(*)(const char *, int), \
glob_t *restrict);
void globfree(glob_t *);
