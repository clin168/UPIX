// !complete
#define CLOCKS_PER_SEC 10000000 // 1 million for XSI
#define NULL 0

struct tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
	int tm_wday;
	int tm_yday;
	int tm_isdst;
};

/* Obselete: PLEASE do not use them! */
char *asctime(const struct tm *);
char *ctime(const time_t *);

clock_t clock(void);
double difftime(time_t, time_t);
struct tm *gmtime(const time_t *);
struct tm *localtime(const time_t);
time_t mktime(struct tm *);
size_t strftime(char *restrict, size_t, const char *restrict, \
const struct tm *restrict);
time_t time(time_t *);

#ifdef _POSIX_SOURCE

#define CLOCK_MONOTONIC 0
#define CLOCK_REALTIME 1

#define TIMER_ABSTIME 1

struct timespec {
	time_t tv_sec;
	long tv_nsec;
};

struct itimerspec {
	struct timespec it_interval;
	struct timespec it_value;
};

/* Obselete: PLEASE do not use them! */
char *asctime_r(const struct tm *restrict, char *restrict);
char *ctime_r(const time_t *, char *);

int clock_getres(clockid_t, struct timespec *);
int clock_gettime(clockid_t, struct timespec *);
int clock_nanosleep(clockid_t, int, const struct timespec *, struct timespec *);
int clock_settime(clockid_t, const struct timespec *);
struct tm *gmtime_r(const time_t *restrict, struct tm *restrict);
struct tm *localtime_r(const time_t *restrict, struct tm *restrict);
int nanosleep(const struct timespec *, struct timespec *);
size_t strftime_l(char *restrict, size_t, const char *restrict, \
const struct tm *restrict, locale_t);
int timer_create(clockid_t, struct sigevent *restrict, timer_t *restrict);
int timer_delete(timer_t);
int timer_getoverrun(timer_t);
int timer_gettime(timer_t, struct itimerspec *);
int timer_settime(timer_t, int, const struct itimerspec *restrict, \
struct itimerspec *restrict);
void tzset(void);

extern char *tzname[];

#endif

#ifdef _POSIX_CPUTIME
#define CLOCK_PROCESS_CPUTIME_ID 2
int clock_getcpuclockid(pid_t, clockid_t *);
#endif

#ifdef _POSIX_MONOTONIC_CLOCK
 #define CLOCK_MONOTONIC 3
#endif

#ifdef _POSIX_THREAD_CPUTIME
 #define CLOCK_THREAD_CPUTIME_ID 4
#endif

#ifdef _XOPEN_SOURCE
struct tm *getdate(const char *);
char *strptime(const char *restrict, const char *restrict, \
struct tm *restrict);

extern int getdate_err; // Temporary solution
extern int daylight;
extern long timezone;
#endif
