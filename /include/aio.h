#include <signal.h> // !complete
#include <time.h>

#include <sys/types.h>

struct aiocb {
	int aio_fildes, aio_reqprio, aio_lio_opcode;
	off_t aio_offset;
	volatile void *aio_buf;
	size_t aio_nbytes;
	struct sigevent aio_sigevent;
};

#define AIO_CANCELED 0
#define AIO_NOTCANCELED 1
#define AIO_ALLDONE 2

#define LIO_READ 0
#define LIO_WRITE 1
#define LIO_NOP 2

#define LIO_WAIT 0
#define LIO_NOWAIT 1

int aio_read(struct aiocb *);
int aio_write(struct aiocb *);
int aio_error(const struct aiocb *);
ssize_t aio_return(struct aiocb *);
int aio_cancel(int, struct aiocb *);
int aio_suspend(const struct aiocb *const [], int, const struct timespec *);
int lio_listio(int, struct aiocb *restrict const [restrict], int,
struct sigevent *restrict);

#ifdef _POSIX_FSYNC|_POSIX_SYNCHRONIZED_IO
int aio_fsync(int, struct aiocb *);
#endif
