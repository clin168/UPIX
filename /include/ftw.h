#ifdef _XOPEN_SOURCE // !complete

#define FTW_F 1
#define FTW_D 2
#define FTW_DNR 3
#define FTW_DP 4
#define FTW_NS 5
#define FTW_SL 6
#define FTW_SLN 7

#define FTW_PHYS 1
#define FTW_MOUNT 2
#define FTW_DEPTH 3
#define FTW_CHDIR 4

struct FTW{
	int base;
	int level;
};

/* Obsolete: PLEASE do not use! */
int ftw(const char *, int (*)(const char *, const struct stat *, int), int);
int nftw(const char *, \
int (*)(const char *, const struct stat *, int, struct FTW *), int, int);

#endif
