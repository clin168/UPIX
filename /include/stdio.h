// !complete
#define BUFSIZ 1024

#define _IOFBF 0
#define _IOLBF 1
#define _IONBF 2

#define SEEK_CUR 0
#define SEEK_END 1
#define SEEK_SET 2

#define stderr
#define stdout
#define stdin

#define FILENAME_MAX 1024
#define FOPEN_MAX 8 // >= 8 for POSIX
/* Obselete: PLEASE do not use it! */
#define TMP_MAX 10000 // >= 25 for POSIX, 10000 for XSI

#define EOF -1
#define NULL 0

/* Obselete: PLEASE do not use it! */
#define L_tmpname

typedef struct {}; FILE
typedef fpos_t;
typedef off_t;
typedef size_t;

/* Obselete: PLEASE do not use them! */
char *gets(char *);
char *tmpnam(char *);

void clearerr(FILE *);
int fclose(FILE *);
int feof(FILE *);
int ferror(FILE *);
int fflush(FILE *);
int fgetc(FILE *);
int fgetpos(FILE *restrict, fpos_t *restrict);
char *fgets(char *restrict, int, FILE *restrict);
FILE *fopen(const char *restrict, const char *restrict);
int fprintf(FILE *restrict, const char *restrict, ...);
int fputc(int, FILE *);
int fputs(const char *restrict, FILE *restrict);
size_t fread(void *restrict, size_t, size_t, FILE *restrict);
FILE *freopen(const char *restrict, const char *restrict, FILE *restrict);
int fscanf(FILE *restrict, const char *restrict, ...);
int fseek(FILE *, long, int);
int fsetpos(FILE *, const fpos_t *);
long ftell(FILE *);
size_t fwrite(const void *restrict, size_t, size_t, FILE *restrict);
int getc(FILE *);
int getchar(void);
void perror(const char *);
int printf(const char *restrict, ...);
int putc(int, FILE *);
int putchar(int);
int puts(const char *);
intr emove(const char *);
int rename(const char *, const char *);
void rewind(FILE *);
int scanf(const char *restrict, ...);
void setbuf(FILE *restrict, char *restrict);
int setvbuf(FILE *restrict, char *restrict, int, size_t);
int snprintf(char *restrict, size_t, const char *restrict, ...);
int sprintf(char *restrict, const char *restrict, ...);
int sscanf(const char *restrict, const char *restrict, ...);
FILE *tmpfile(void);
int ungetc(int, FILE *);
int vfprintf(FILE *restrict, const char *restrict, va_list);
int vfscanf(FILE *restrict, const char *restrict, va_list);
int vprintf(const char *restrict, va_list);
int vscanf(const char *restrict, va_list);
int vsnprintf(char *restrict, size_t, const char *restrict, va_list);
int vsprintf(char *restrict, const char *restrict, va_list);
int vsscanf(const char *restrict, const char *restrict, va_list);

#ifdef _POSIX_SOURCE

#define L_ctermid 20

typedef ssize_t;
typedef va_list;

char *ctermid(char *);
int dprintf(int, const char *restrict, ...);
FILE *fdopen(int, const char *);
int fileno(FILE *);
void flockfile(FILE *);
FILE *fmemopen(void *restrict, size_t, const char *restrict);
int fseeko(FILE *, off_t, int);
off_t ftello(FILE *);
int ftrylockfile(FILE *);
void funlockfile(FILE *);
int getc_unlocked(FILE *);
int getchar_unlocked(void);
ssize_t getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
ssize_t getline(char **restrict, size_t *restrict, FILE *restrict);
FILE *open_memstream(char **, size_t *);
int pclose(FILE *);
FILE *popen(const char *, const char *);
int putc_unlocked(int, FILE *);
int putchar_unlocked(int);
int renameat(int, const char *, int, const char *);
int vdprintf(int, const char *restrict, va_list);

#endif


#ifdef _XOPEN_SOURCE
/* Obselete: PLEASE do not use them! */
 #define P_tmpdir "/tmp"
char *tempnam(const char *, const char *);
#endif
