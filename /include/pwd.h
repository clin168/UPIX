#include "private/ugid_t.h"
#include "private/size_t.h"

struct passwd {
    char *pw_name;  // Login name
    uid_t pw_uid;   // UID
    gid_t pw_gid;   // GID
    char *pw_dir;   // Initial working directory
    char *pw_shell; // Program as the shell
};

#ifdef _XOPEN_VERSION
 void endpwent(void);
 struct passwd *getpwent(void);
 void setpwent(void);
#endif

struct passwd *getpwnam(const char *);
int getpwnam_r(const char *, struct passwd *, char *, size_t, \ 
struct passwd **);
struct passwd *getpwuid(uid_t);
int getpwuid_r(uid_t, struct passwd *, char *, size_t, struct passwd **);