#include <sys/types.h> // !complete
// Define DIR somehow
typedef struct {
	ino_t d_ino;
	char d_name[NAME_MAX]; //specify size
}dirent;

typedef DIR;

int alphasort(const struct dirent **, const struct dirent **);
int closedir(DIR *);
int dirfd(DIR *);
DIR *fdopendir(int);
DIR *opendir(const char *);
struct dirent *readdir(DIR *);
int readdir_r(DIR *restrict, struct dirent *restrict, \
struct dirent **restrict);
void rewinddir(DIR *);
int scandir(const char *, struct dirent ***, int (*)(const struct dirent *), \
int (*)(const struct dirent **, const struct dirent **));

#ifdef _XOPEN_SOURCE
void seekdir(DIR *, long);
long telldir(DIR *);
#endif
