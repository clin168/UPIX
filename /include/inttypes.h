#include <stdint.h> // !complete

PRId
PRIdLEASTN
PRIiLEASTN
PRIdFASTN
PRIiFASTN
PRIdMAX
PRIiMAX PRIdPTR
PRIiPTR

typedef struct {
	
}imaxdiv_t;

intmax_t imaxabs(intmax_t);
imaxdiv_t imaxdiv(intmax_t, intmax_t);
intmax_t strtoimax(const char *restrict, char **restrict, int);
uintmax_t strtoumax(const char *restrict, char **restrict, int);
intmax_t wcstoimax(const wchar_t *restrict, wchar_t **restrict, int);
uintmax_t wcstoumax(const wchar_t *restrict, wchar_t **restrict, int);
