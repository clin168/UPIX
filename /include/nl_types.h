#define NL_SETD 1
#define NL_CAT_LOCALE 2

typedef nl_catd;
typedef nl_item;

int catclose(nl_catd);
char *catgets(nl_catd, int, int, const char *);
nl_catd catopen(const char *, int);
