#ifdef NDEBUG
#define assert(expr) ((void)0)
#else
void check_assertion(int);
#define assert(expr) check_assertion(expr)
#endif